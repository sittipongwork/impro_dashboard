<div class="caption left-align app-caption-left-slide-mainpage">
  <h3 class="app-title-name">คณะทำงาน 4 คณะประกอบด้วย</h3>
  <h5 class="light app-title-content text-lighten-3">1. คณะทำงานด้านการจัดการพลังงาน (ทกพ.)</h5>
  <h5 class="light app-title-content text-lighten-3">2. คณะผู้ตรวจประเมินการจัดการพลังงาน</h5>
  <h5 class="light app-title-content text-lighten-3">3. คณะทำงานส่งเสริมการอนุรักษ์พลังงาน</h5>
  <h5 class="light app-title-content text-lighten-3">4. คณะผู้ส่งเสริมและสนับสนุนการประหยัดพลังงา</h5>
</div>
<div class="caption right-align app-caption-right-slide-mainpage">
  <div class="col s6"><img src="<?php echo base_url('assets/img/energysave.png'); ?>" style="background-size: cover;height: 700px;width: auto;background-position: inherit;" class="responsive-img" /></div>
</div>
