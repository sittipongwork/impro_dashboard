	<script src="<?php echo base_url('assets/bower_components/highcharts/highcharts.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/MainPageSlideEnergy.js') ?>"></script>
	<script type="text/javascript">
			$(document).ready(function(){
				$('.slider').slider({
					full_width: true, //เต็มจอ
					indicators: true //bullet สไล
				});
			});
			function pauseSlide(){
				$('.slider').slider('pause');
			}
			function nextSlide(){
				$('.slider').slider('next');
			}
			function prevSlide(){
				$('.slider').slider('prev');
			}
			function resumeSlide(){
				$('.slider').slider('start');
			}
			function reloadSlide() {
					alert('Refresh Screen!!')
			    location.reload();
			}
	</script>
	<div class="slider fullscreen">
    <ul class="slides background-white">
			<li>
        <img src="">
        <?php $this->load->view('dashboard/slide/intro') ?>
      </li>
			<li>
        <img src="">
        <?php $this->load->view('dashboard/slide/mapbot') ?>
      </li>
      <li>
        <img src="">
        <div class="caption left-align app-caption-left-slide-mainpage">
          <h3 class="app-title-name">ข้อมูลการ</h3>
          <h5 class="light app-title-content text-lighten-3">อาคาร 1</h5>
        </div>
				<div class="caption right-align app-caption-right-slide-mainpage">
          <div id="show_building_all" style="height: 600px;width: 900px; margin: 0 auto;float: right;"></div>
        </div>
      </li>
			<li>
        <img src="">
        <div class="caption left-align app-caption-left-slide-mainpage">
          <h3 class="app-title-name">ข้อมูลการ</h3>
          <h5 class="light app-title-content text-lighten-3">อาคาร 1</h5>
        </div>
				<div class="caption right-align app-caption-right-slide-mainpage">
          <div id="show_building_one" style="height: 600px;width: 900px; margin: 0 auto;float: right;"></div>
        </div>
      </li>
    </ul>
  </div>
	<div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red waves-effect waves-light">
      <i class="material-icons">settings</i>
    </a>
    <ul>
      <li><a class="btn-floating waves-effect waves-light red" onclick="reloadSlide()"><i class="material-icons">replay</i></a></li>
			<li><a class="btn-floating waves-effect waves-light red" onclick="pauseSlide()"><i class="material-icons">pause</i></a></li>
			<li><a class="btn-floating waves-effect waves-light red" onclick="resumeSlide()"><i class="material-icons">play_arrow</i></a></li>
			<li><a class="btn-floating waves-effect waves-light red" onclick="prevSlide()"><i class="material-icons">fast_rewind</i></a></li>
			<li><a class="btn-floating waves-effect waves-light red" onclick="nextSlide()"><i class="material-icons">fast_forward</i></a></li>
    </ul>
  </div>
