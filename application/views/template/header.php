<!DOCTYPE html>
<html lang="th">
<head>
	<meta charset="utf-8">
	<title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--<link rel="shortcut icon" type="image/png" href="/public/img/favicon.png"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bower_components/materialize/dist/css/materialize.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/app.min.css') ?>">
    <script src="<?php echo base_url('assets/bower_components/jquery/jquery-2.2.4.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/bower_components/materialize/dist/js/materialize.min.js') ?>"></script>
</head>
<body>
<div class="navbar-fixed">
	<!-- Dropdown Structure -->
	<ul id="dropdown1" class="dropdown-content">
	  <li><a href="#!">one</a></li>
	  <li><a href="#!">two</a></li>
	  <li class="divider"></li>
	  <li><a href="#!">three</a></li>
	</ul>
	<nav>
	  <div class="nav-wrapper app-header-padding">
	    <a href="/" class="brand-logo">Logo</a>
	    <ul class="right hide-on-med-and-down">
	      <li><a href="sass.html">Sass</a></li>
	      <li><a href="badges.html">Components</a></li>
	      <!-- Dropdown Trigger -->
	      <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
	    </ul>
	  </div>
	</nav>
</div>
<script type="text/javascript">
	$(".dropdown-button").dropdown();
</script>
