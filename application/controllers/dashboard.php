<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'controllers/AppController.php');

class Dashboard extends AppController {

	public function index()
	{
    $this->app_views('dashboard/index',[]);
	}

}
