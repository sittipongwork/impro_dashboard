<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AppController extends CI_Controller {

  public function app_views($view,$data){
    $this->load->view('template/header');
		$this->load->view($view,$data);
    $this->load->view('template/footer');
  }

}
